const express = require("express");
const router = express.Router(); 
const Course = require("../models/product.js");
const productController = require("../controllers/productControllers.js");
const auth = require("../auth.js");


//------------------------------------------------------------------------------------

router.post("/addProduct", auth.verify, (req, res) =>{
	const newData = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(req.body, newData).then(
		resultFromController => {
			res.send(resultFromController)
		})
})


//------------------------------------------------------------------------------------

router.get("/", (req, res) =>{
	productController.getActiveProducts().then(
		resultFromController => {
			res.send(resultFromController)
		})
})

// day 3 capstone ---------------------------------------------------------------------


router.get("/:productId", (req, res) => {		
	productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController))
})

//--------------------------------------------------------------------------------------

router.put("/:productId/update", auth.verify, (req,res) => 
{
	const newData = {
		product: req.body, 	//req.headers.authorization contains jwt
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(req.params.productId, newData).then(resultFromController => {
		res.send(resultFromController)
	})
})

// archive products ------------------------------------------------------------------

router.patch("/:productId/archive", auth.verify, (req, res) => {
    productController.archiveProduct(req.params.productId).then(resultFromController => {
        res.send(resultFromController)
    });
});




module.exports = router;
