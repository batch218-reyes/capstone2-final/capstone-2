const express = require("express");
const router = express.Router();
const User = require("../models/user.js");
const userController =  require("../controllers/userController.js");
const auth = require("../auth.js");

//------------------------------------------------------------------------------------

router.post("/register",(req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

//------------------------------------------------------------------------------------

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Check Email
router.post("/checkEmail", userController.checkEmail)

//-----------------------------------------------------------------------------
router.get("/details", auth.verify, userController.getUser);


module.exports = router;