// start --------------------------------------------------------------------------

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();

// routers-------------------------------------------------------------------------

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

//middlewares-----------------------------------------------------------------------
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Initializing the routes----------------------------------------------------------

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);


// Connect to mongodb---------------------------------------------------------------

mongoose.connect("mongodb+srv://admin:admin@batch218-coursebooking.5uymap1.mongodb.net/capstone2?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

// Connect to our MongoDB Database---------------------------------------------------

mongoose.connection.once('open', () => console.log('Now connected to Gabriel DB Atlas.'));


app.listen(process.env.PORT || 4000, () => {console.log(`API is now online on port ${process.env.PORT || 4000 }`)});
