const jwt = require("jsonwebtoken");
const secret = "Capstone2";

// Token Creation
module.exports.createAccessToken = (user) => {
	// payload
	const data = {
		id: user.id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}



//verify ---------------------------------------------------------------------------


module.exports.verify = (request, response, next) => {
	// Get JWT(JSON web token) from postman
	let token = request.headers.authorization

	if(typeof token !== "undefined"){
			console.log(token);

			//remove unecessary characters("Bearer  "") from the token
			token = token.slice(7, token.length);

			return jwt.verify(token, secret, (error, data) => {
				if(error){
					return response.send({
						auth: "Failed."
					})
				}
				else {
					next()
				}
			})
	}
	else{
		return null;
	}
}

//decryption ------------------------------------------------------------------------


module.exports.decode = (token) => {
	if(typeof token !== "undefined"){

			//remove unecessary characters("Bearer  "") from the token
		token = token.slice(7, token.length);
	}
	return jwt.verify(token, secret, (error, data) => {
		if (error){
			return null
		}
		else {
			return jwt.decode(token, {complete:true}).payload
		}
	})
}

	