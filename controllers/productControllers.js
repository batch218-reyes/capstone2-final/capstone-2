const Product = require("../models/product.js");

//------------------------------------------------------------------------------------

module.exports.addProduct = (reqBody, newData) => {
if(newData.isAdmin == true){
	let newProduct = new Product ({
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price

	})
	return newProduct.save().then((newProduct, error) => {
		if(error){
				return error;
		}
		else{
			return newProduct;
		}
	})
}else{
	let message = Promise.resolve('User must be ADMIN to add a new product');
		return message.then((value) => {return value})
	}
}

//------------------------------------------------------------------------------------

module.exports.getActiveProducts = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	})
}

// day 3 capstone ---------------------------------------------------------------------

module.exports.getProduct = (productId) => {
	return Product.findById(productId).then(result => {
		return result;
	})
}


// get product ------------------------------------------------------------------------
module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		// update code
		return Product.findByIdAndUpdate(productId , 
			{			//req.body  
				name: newData.product.name,
				description: newData.product.description,
				price: newData.product.price
			}
		).then((updateProduct, error) => {
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
	}
}

//archive product---------------------------------------------------------------------------

module.exports.archiveProduct = (productId) => {
    return Product.findByIdAndUpdate(productId, {
        isActive: false
    })
    .then((archiveProduct, error) => {
        if(error){
            return false
        } 

        return {
            message: "Product archived successfully!"
        }
    })
};

