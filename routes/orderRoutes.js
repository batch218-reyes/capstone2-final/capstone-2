const express = require("express");
const router = express.Router(); 
const Course = require("../models/order.js");
const orderController = require("../controllers/orderController.js");
const auth = require("../auth.js");

//-------------------------------------------------------------------------------


router.post("/new-order", auth.verify, (req, res)=> {
    const data = {
        productId: req.body.productId,
        quantity: req.body.quantity,
        userId: auth.decode(req.headers.authorization).id, 
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    }
    orderController.createOrder(data).then(resultFromController=> res.send(resultFromController))
});


module.exports = router;
//---------------------------------------------------------------------------

router.get('/allOrders', auth.verify, (req, res) => {
    const data = {
        user: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    orderController.showOrder(data).then(resultFromController=> res.send(resultFromController))
})
