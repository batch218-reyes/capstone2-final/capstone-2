const Order = require("../models/order.js");
const Product = require('../models/product.js');
const auth = require('../auth');
const bcrypt = require('bcrypt');

module.exports.createOrder = (data) => {

    if (data.isAdmin == false) {
        let newOrder = new Order ({
            products: [{
                productId: data.productId,
                quantity: data.quantity}],
                userId: data.userId,
        })

        return newOrder.save().then((newOrder, err) => {
            if (err) {
                return err;
            }
            else {
                return newOrder;
            }
        })
    }
    else{
        let message = Promise.resolve('User must be a regular user to create an order');
        return message.then(value => value)
    }

}

//show all order--------------------------------------------------------
module.exports.showOrder = (data) => {
    if (data.isAdmin == true){
        return Order.find({}).then(result => result)
    }
    else{
        let message = Promise.resolve('User must be ADMIN to access this');
        return message.then(value => value)
    }

}
